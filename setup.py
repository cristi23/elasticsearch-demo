#!/usr/bin/env python
from setuptools import setup, find_packages

dependencies = [
    'requests',
    'python-memcached',
    'elasticsearch>=1.0.0,<2.0.0',
    'Flask',
]

dependency_links = []

setup(
    name='elasticsearch_demo',
    version='0.1.0',
    description='Playground for learning how to use Elasticsearch',
    long_description='',
    author='Cristi Scoarta',
    author_email='cristi.scoarta@yahoo.com.com ',
    url='https://bitbucket.org/cristi23/elasticsearch-demo/',
    packages=find_packages(),
    include_package_data=True,
    install_requires=dependencies,
    dependency_links=dependency_links,
)
