# README #

This is a search service prototype made with ElasticSearch

### Set up ###

* Download elasticsearch (from https://www.elastic.co/downloads/elasticsearch)
* Start elasticsearch (by running bin/elasticsearch from the elasticsearch directory)
* Create a virtualenv and activate it (Steps 1 and 2 from http://docs.python-guide.org/en/latest/dev/virtualenvs/)
* Clone this repository (git clone https://bitbucket.org/cristi23/elasticsearch-demo.git) and
* Run the following commands:
    * cd elasticsearch-demo
    * python setup.py develop
    * cd es_demo
    * python index.py
    * python web.py
* Go to localhost:5000 in your web browser
* Enjoy!