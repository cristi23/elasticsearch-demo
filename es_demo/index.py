from es_demo.data import get_programs
from elasticsearch import Elasticsearch
from elasticsearch.client.indices import IndicesClient
es = Elasticsearch()
es_indices_client = IndicesClient(es)


def index_programs():
    programs = get_programs('android')

    # create the 'pbs' index (if it already exists, delete it manually; I
    # could have done it here, but I chose not to)
    es_indices_client.create(index="pbs")

    # create the program-episode mapping
    es_indices_client.put_mapping(index="pbs", doc_type='episode', body={
        "_parent": {
            "type": "program"
        }
    })

    for program in programs:
        slug = program.get('slug')

        doc = {
            'slug': slug,
            'title': program.get('title'),
            'description': program.get('description'),
        }
        res = es.index(index="pbs", doc_type='program', id=slug, body=doc)
        print res

        for episode in program.get('seasons')[0].get('episodes', []):
            doc = {
                'id': episode.get('id'),
                'title': episode.get('title'),
                'description': episode.get('description'),
                'duration': episode.get('duration'),
                'video_type': episode.get('video_type'),
                'images': {
                    'mezzanine': episode.get('images', {}).get('mezzanine'),
                }
            }
            res = es.index(index="pbs", doc_type="episode",
                           id=episode.get('id'), body=doc, parent=slug)
            print res

if __name__ == '__main__':
    index_programs()
