from flask import Flask
from flask.templating import render_template
from flask.globals import request
from es_demo.search import search_videos


app = Flask(__name__)


@app.route("/")
def search():
    query = request.args.get('q')
    if not query:
        return render_template('search_results.html', no_query=True)

    duration_ranges = request.args.getlist('filter_duration')
    video_type = request.args.get('filter_video_type')
    program = request.args.get('filter_program')
    size = 10
    start = int(request.args.get('start', 0))

    search_result = search_videos(query, duration_ranges=duration_ranges,
                                  video_type=video_type, program=program,
                                  size=size, start=start)
    hits = search_result.get('hits', [])
    aggs = search_result.get('aggregations', [])
    total = hits.get('total')

    # create the filters which are going to be displayed on the page
    filters = []
    for key, agg in aggs.items():
        buckets = []
        allow_multiple = key == 'duration'
        for bucket in agg.get('buckets', []):
            b_key = bucket.get('key')
            _active = 'filter_' + key + '=' + b_key in request.full_path
            _disabled = bucket.get('doc_count') == 0 and not _active if not allow_multiple else False
            _url = request.full_path
            if _disabled:
                _url = _url + '#'
            elif _active:
                _url = _url.replace('&filter_' + key + '=' + b_key, '')
            else:
                _url = _url + '&filter_' + key + '=' + b_key
            buckets.append({
                'display': b_key,
                'count': bucket.get('doc_count'),
                'value': b_key,
                'active': _active,
                'disabled': _disabled,
                'url': _url,
            })
        # there's no point in adding this filter since all the options were
        # already filtered out by other filters
        if len(buckets):
            filters.append({
                'display': key,
                'buckets': buckets,
                'allow_multiple': allow_multiple,
            })

    _args = {
        arg: value for arg, value in request.args.items() if arg != 'start'
    }

    # create the pagination links
    _path = request.path + '?' + '&'.join([key + '=' + val for key, val in _args.items()])
    prev_page = (_path + '&start=%s' % (start - size)) if start - size >= 0 else '#'
    next_page = (_path + '&start=%s' % (start + size)) if start + size <= total else '#'

    return render_template('search_results.html', results=hits.get('hits', []),
                           filters=filters, prev_page=prev_page,
                           next_page=next_page, total=total)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
