import requests
import json

from es_demo.settings import CONTENT_SERVICE_AUTH, CONTENT_SERVICE_PROGRAMS_URL


def get_programs(client):
    try:
        with open('/tmp/programs.json', 'r') as f:
            data = json.loads(f.read())
    except IOError:
        data = None

    if data:
        print 'from cache'
        return data
    else:
        print 'data is None'
        response = requests.get(CONTENT_SERVICE_PROGRAMS_URL % client,
                                auth=CONTENT_SERVICE_AUTH)
        if response.status_code == 200:
            data = []
            for _, content in response.json().get('collections', {}).get('all-programs-az', {}).get('content', {}).items():
                for program in content:
                    res = requests.get(program.get('URI'),
                                       auth=CONTENT_SERVICE_AUTH)
                    if res.status_code == 200:
                        res_obj = res.json()
                        program_obj = res_obj.pop('object')
                        videos = []
                        videos += res_obj.get('collections', {}).get('clips', {}).pop('content', [])
                        videos += res_obj.get('collections', {}).get('previews', {}).pop('content', [])
                        videos += res_obj.get('collections', {}).get('episodes', {}).pop('content', [])
                        program_obj['seasons'] = [{
                            'episodes': videos
                        }]
                        data.append(program_obj)
                        print 'found program %s with %s videos' % (program_obj.get('slug'), len(videos))

            print 'saving to cache'
            with open('/tmp/programs.json', 'w') as f:
                f.write(json.dumps(data))
            return data
        return []
