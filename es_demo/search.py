from elasticsearch import Elasticsearch


def search_videos(query, program=None, video_type=None, duration_ranges=None,
                  size=10, start=0):
    es = Elasticsearch()
    query = {
        "filtered": {
            "query": {
            "bool": {
              "should": {
                "has_parent": {
                  "parent_type": "program",
                  "query": {
                    "bool": {
                      "should": {
                        "multi_match": {
                          "query": query,
                          "fields": [
                            "title^10",
                            "description"
                          ]
                        }
                      },
                      "must": {
                        "match_all": {}
                      }
                    }
                  },
                  "inner_hits": {}
                }
              },
              "must": {
                "multi_match": {
                  "query": query,
                  "type": "best_fields",
                  "fields": [
                    "title^5",
                    "description"
                  ],
                  "tie_breaker": 0.3,
                  "minimum_should_match": "30%"
                }
              }
            }
          },
          "filter": {}
        }
      }
    filters = []
    if video_type:
        filters.append({
            "term": {
                "video_type": video_type
            }
        })
    if duration_ranges:
        def _float(val):
            if val == '*':
                val = '0'
            return int(float(val))

        range_filters = []
        for duration_range in duration_ranges:
            _from, _to = map(_float, duration_range.split('-'))
            range_filters.append({
                "range": {
                    "duration": {
                        "from": _from,
                        "to": _to,
                    }
                }
            })

        filters.append({
            "or": range_filters,
        })
    if program:
        filters.append({
            "term": {
                "_parent": program,
            }
        })

    if filters:
        query["filtered"]["filter"] = {
            "and": filters
        }
    aggs = {
        "duration": {
          "range": {
            "field": "duration",
            "ranges": [
              {
                "to": 601
              },
              {
                "from": 601,
                "to": 1801
              },
              {
                "from": 1801,
                "to": 3601
              },
              {
                "from": 3601
              }
            ]
          }
        },
        "video_type": {
          "terms": {
            "field": "video_type",
            "order": {
                "_term": "asc"
            }
          }
        },
        "program": {
          "terms": {
            "field": "_parent",
            "size": 20,
#             "order": {
#                 "_term": "asc"
#             }
          }
        }
      }

    body = {
        "query": query,
        "aggs": aggs
    }

    return es.search(index='pbs', doc_type='episode', body=body,
                     size=size, from_=start)
